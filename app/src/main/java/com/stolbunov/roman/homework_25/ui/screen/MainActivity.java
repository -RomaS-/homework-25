package com.stolbunov.roman.homework_25.ui.screen;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.stolbunov.roman.homework_25.R;
import com.stolbunov.roman.homework_25.data.PermissionData;
import com.stolbunov.roman.homework_25.data.PermissionDataFactory;
import com.stolbunov.roman.homework_25.ui.adapters.OnItemClickListener;
import com.stolbunov.roman.homework_25.ui.adapters.PermissionDataAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnItemClickListener {
    private final int REQUEST_PERMISSION = 848;
    private final String PERMISSION_GRANTED = "PERMISSION GRANTED";
    private final String PERMISSION_DENIED = "PERMISSION DENIED";
    private PermissionDataAdapter adapter;
    private int positionElement;
    private PermissionData permissionData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<PermissionData> data = getStatusForPermissions(PermissionDataFactory.getListPermissionData());
        adapter = getAdapter(data);
        RecyclerView.LayoutManager manager = getManager();

        initRecyclerView(adapter, manager);

    }

    private void initRecyclerView(PermissionDataAdapter adapter, RecyclerView.LayoutManager manager) {
        RecyclerView rvPermission = findViewById(R.id.rv_permission);
        rvPermission.setAdapter(adapter);
        rvPermission.setLayoutManager(manager);
    }

    @NonNull
    private LinearLayoutManager getManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    @NonNull
    private PermissionDataAdapter getAdapter(List<PermissionData> data) {
        PermissionDataAdapter adapter = new PermissionDataAdapter(data);
        adapter.setListener(this);
        return adapter;
    }


    @Override
    public void onItemClick(PermissionData permissionData, int position) {
        positionElement = position;
        this.permissionData = permissionData;

        if (checkIsPermission(permissionData)) {

        } else {
            requestPermission(permissionData);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionData.setPermissionStatus(PERMISSION_GRANTED);
            adapter.notifyItemChanged(positionElement);
        }
    }

    private void requestPermission(PermissionData permissionData) {
        ActivityCompat.requestPermissions(this,
                new String[]{permissionData.getPermissionFullName()}, REQUEST_PERMISSION);
    }

    private List<PermissionData> getStatusForPermissions(List<PermissionData> permissionData) {
        List<PermissionData> data = new ArrayList<>(permissionData);
        for (PermissionData data1 : data) {
            if (checkIsPermission(data1)) {
                data1.setPermissionStatus(PERMISSION_GRANTED);
            } else {
                data1.setPermissionStatus(PERMISSION_DENIED);
            }
        }
        return data;
    }

    private boolean checkIsPermission(PermissionData data) {
        return ContextCompat.checkSelfPermission(this, data.getPermissionFullName())
                == PackageManager.PERMISSION_GRANTED;
    }
}
