package com.stolbunov.roman.homework_25.ui.adapters;

import com.stolbunov.roman.homework_25.data.PermissionData;

public interface OnItemClickListener {
    void onItemClick(PermissionData permissionData, int position);
}
