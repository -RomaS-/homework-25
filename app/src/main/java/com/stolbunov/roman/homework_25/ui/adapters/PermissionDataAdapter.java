package com.stolbunov.roman.homework_25.ui.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stolbunov.roman.homework_25.R;
import com.stolbunov.roman.homework_25.data.PermissionData;

import java.util.List;

public class PermissionDataAdapter extends RecyclerView.Adapter<PermissionDataAdapter.PermissionDataViewHolder> {
    private List<PermissionData> data;
    private OnItemClickListener listener;

    public PermissionDataAdapter(List<PermissionData> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public PermissionDataViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_permission_info, viewGroup, false);
        return new PermissionDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PermissionDataViewHolder permissionDataViewHolder, int position) {
        permissionDataViewHolder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }



    class PermissionDataViewHolder extends RecyclerView.ViewHolder {
        AppCompatTextView permissionName;
        AppCompatTextView permissionStatus;

        public PermissionDataViewHolder(@NonNull View itemView) {
            super(itemView);

            permissionName = itemView.findViewById(R.id.permission_name);
            permissionStatus = itemView.findViewById(R.id.permission_status);
        }

        public void bind(PermissionData permissionData) {
            itemView.setOnClickListener(v -> onClick(permissionData));

            permissionName.setText(permissionData.getPermissionName());
            permissionStatus.setText(permissionData.getPermissionStatus());
        }

        private void onClick(PermissionData permissionData) {
            if (listener != null) {
                listener.onItemClick(permissionData, getAdapterPosition());
            }
        }
    }
}
