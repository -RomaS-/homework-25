package com.stolbunov.roman.homework_25.data;

import android.Manifest;

import java.util.LinkedList;
import java.util.List;


public class PermissionDataFactory {
    private static List<String> permissionNames;
    private static List<String> permissionFullNames;

    static {
        createFullNameList();
        createNameList();
    }

    public static List<PermissionData> getListPermissionData() {
        List<PermissionData> list = new LinkedList<>();
        for (int i = 0; i < permissionNames.size(); i++) {
            list.add(new PermissionData(
                    permissionNames.get(i),
                    permissionFullNames.get(i)));
        }
        return list;
    }

    private static void createNameList() {
        permissionNames = new LinkedList<>();
        permissionNames.add("CAMERA");
        permissionNames.add("RECORD AUDIO");
        permissionNames.add("WRITE EXTERNAL STORAGE");
        permissionNames.add("READ EXTERNAL STORAGE");
        permissionNames.add("ACCESS FINE LOCATION");
        permissionNames.add("BLUETOOTH");
    }

    private static void createFullNameList() {
        permissionFullNames = new LinkedList<>();
        permissionFullNames.add(Manifest.permission.CAMERA);
        permissionFullNames.add(Manifest.permission.RECORD_AUDIO);
        permissionFullNames.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionFullNames.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissionFullNames.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissionFullNames.add(Manifest.permission.BLUETOOTH);
    }

}
