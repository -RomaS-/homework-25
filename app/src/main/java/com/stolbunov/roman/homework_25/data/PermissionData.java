package com.stolbunov.roman.homework_25.data;

public class PermissionData {
    private String permissionName;
    private String permissionFullName;
    private String permissionStatus;

    public PermissionData(String permissionName, String permissionFullName) {
        this.permissionName = permissionName;
        this.permissionFullName = permissionFullName;
    }

    public String getPermissionName() {
        return permissionName;
    }

    public String getPermissionFullName() {
        return permissionFullName;
    }

    public String getPermissionStatus() {
        return permissionStatus;
    }

    public void setPermissionStatus(String permissionStatus) {
        this.permissionStatus = permissionStatus;
    }
}
